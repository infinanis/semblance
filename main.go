package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/itchyny/json2yaml"

	"codeberg.org/infinanis/semblance/internal/api"
	"codeberg.org/infinanis/semblance/internal/cli"
)

func main() {
	var err error

	endpoint, err := api.Data.FindEndpoint(cli.Section, cli.Endpoint)
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}

	filetype := api.FiletypeMap[cli.Section]

	req, err := api.MakeRequest(endpoint, cli.Args, filetype)
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}

	res, err := api.Do(req)
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}

	var file *os.File

	if cli.Outfile != "" {
		if _, err = os.Stat(cli.Outfile); err == nil {
			fmt.Fprintf(
				os.Stderr,
				"File '%s' already exists\nRefusing to write over it!",
				cli.Outfile,
			)
			os.Exit(1)
		}

		if _, err = os.Stat(filepath.Dir(cli.Outfile)); err != nil {
			fmt.Fprint(os.Stderr, err)
			os.Exit(1)
		}

		file, err = os.Create(cli.Outfile)
		if err != nil {
			fmt.Fprint(os.Stderr, err)
			os.Exit(1)
		}
	} else {
		file = os.Stdout
	}

	switch filetype {
	case api.JSON:
		if cli.DoYaml {
			if err = json2yaml.Convert(file, res); err != nil {
				fmt.Fprint(os.Stderr, err)
				os.Exit(1)
			}
		} else {
			var buf bytes.Buffer
			resBytes, _ := io.ReadAll(res)
			if err = json.Indent(&buf, resBytes, "", "  "); err != nil {
				fmt.Fprint(os.Stderr, err)
				os.Exit(1)
			}
			if _, err = file.Write(buf.Bytes()); err != nil {
				fmt.Fprint(os.Stderr, err)
				os.Exit(1)
			}
		}
	case api.FASTA, api.GFF3, api.BED,
		api.Newick, api.SeqXML, api.PhyloXML,
		api.XML, api.YAML:
		if _, err = io.Copy(file, res); err != nil {
			fmt.Fprint(os.Stderr, err)
			os.Exit(1)
		}
	default:
		fmt.Fprint(os.Stderr, "Encountered unhandled filetype")
		os.Exit(1)
	}
}
