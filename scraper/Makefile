GOCMD=GO111MODULE=on CGO_ENABLED=0 go
GOTEST=$(GOCMD) test
GOVET=$(GOCMD) vet
GOFMT=gofumpt -l -w .
LINT=golangci-lint run
TARGET=scraper

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

.PHONY: default run install build check format lint clean help

default: build

run: ## Compile and run
	@$(MAKE) -s build
	@./bin/$(TARGET) ../api_raw.json true

install: ## Install the binary in $PREFIX
	@install -v -m 0755 bin/$(TARGET) $(PREFIX)

build: ## Build the project
	mkdir -p bin
	$(GOCMD) build -o bin/$(TARGET) .

check: lint format ## Run linters and formatters

format: ## Format source files
	@$(GOFMT)

lint: ## Lint source files
	@$(LINT)

clean: ## Clean the build directory
	rm -rf bin

help: ## Show this help
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)
