package main

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

const server = "https://rest.ensembl.org"

var (
	client        *http.Client
	rateLimitWarn bool
)

func init() {
	client = http.DefaultClient
	rateLimitWarn = false
}

func politeRequest(url string) (*http.Response, error) {
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusTooManyRequests {
		secsFloat, _ := strconv.ParseFloat(resp.Header["Retry-After"][0], 64)
		secs := int(math.Ceil(secsFloat))
		if secs > 2 && !rateLimitWarn {
			fmt.Fprintf(
				os.Stderr,
				"\nWARNING: Large API rate limit detected, consider switching to a less busy network\n",
			)
			rateLimitWarn = true
		}
		time.Sleep(time.Duration(secs) * time.Second)
		resp, err = politeRequest(url)
		if err != nil {
			return nil, err
		}
	}
	return resp, nil
}

func parseArgsTable(table *goquery.Selection, required bool) (params []map[string]any) {
	sel := table.Find("tbody > tr")
	for i := range sel.Nodes {
		childrenSel := sel.Eq(i).Children()
		if strings.Contains(childrenSel.Eq(0).Text(), "callback") {
			continue
		}
		space := regexp.MustCompile(`\s{3,}`)
		example := strings.TrimSpace(childrenSel.Eq(4).Text())
		example = space.ReplaceAllString(example, " | ")
		params = append(params, map[string]any{
			"name":        strings.TrimSpace(childrenSel.Eq(0).Text()),
			"type":        strings.TrimSpace(childrenSel.Eq(1).Text()),
			"description": strings.TrimSpace(childrenSel.Eq(2).Text()),
			"default":     strings.TrimSpace(childrenSel.Eq(3).Text()),
			"example":     example,
		})
	}
	return params
}

func endpointRead(url string) (data map[string]any, err error) {
	res, err := politeRequest(url)
	if err != nil {
		return
	} else if res.StatusCode != 200 {
		err = fmt.Errorf("%s: Error %s", url, res.Status)
		return
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return
	}

	divLead := doc.Find("div.lead")
	tablesSel := divLead.SiblingsFiltered("table").Not(".table-condensed")

	var paramsReq, paramsOpt []map[string]any
	if len(tablesSel.Nodes) == 1 {
		paramsReq = []map[string]any{}
		paramsOpt = parseArgsTable(tablesSel.Eq(0), false)
	} else {
		paramsReq = parseArgsTable(tablesSel.Eq(0), true)
		paramsOpt = parseArgsTable(tablesSel.Eq(1), false)
	}

	method := strings.Split(doc.Find("h1").Text(), " ")[0]
	if method == http.MethodPost {
		bodyParams := map[string]string{}
		tableBodySel := divLead.SiblingsFiltered("table.table-condensed")
		bodyStr := tableBodySel.Find("tbody > tr:nth-child(2) > td:nth-child(2)").Text()
		r, _ := regexp.Compile(`"(?P<key>\w+)"\s?:\s?(?P<val>[[:alpha:]]+)`)
		matchGroups := r.FindAllStringSubmatch(bodyStr, -1)
		for _, matches := range matchGroups {
			for i := 1; i < len(matches); i += 2 {
				bodyParams[matches[i]] = matches[i+1]
			}
		}

		// check for any undocumented body parameters
		for bparam, val := range bodyParams {
			found := false
			for _, param := range paramsReq {
				if param["name"] == bparam {
					found = true
				}
			}
			for _, param := range paramsOpt {
				if param["name"] == bparam {
					found = true
				}
			}
			if !found {
				paramsOpt = append(paramsOpt, map[string]any{
					"name": bparam,
					"type": val,
					"example": tableBodySel.Find("tbody > tr:nth-child(2) > td:nth-child(3)").
						Text(),
					"default": "-",
				})
			}
		}
	}

	urlSplit := strings.Split(url, "/")
	name := urlSplit[len(urlSplit)-1]
	data = map[string]any{
		"name":        name,
		"path":        "/" + strings.Split(doc.Find("h1").Text(), " ")[1],
		"description": divLead.Children().Text(),
		"method":      method,
		"params_req":  paramsReq,
		"params_opt":  paramsOpt,
	}
	return
}

func run(path string, pretty bool) (err error) {
	fmt.Print("Connecting to Ensembl... ")
	res, err := politeRequest(server)
	if err != nil {
		fmt.Println("FAIL")
		return
	} else if res.StatusCode != 200 {
		err = fmt.Errorf("%s: Error %s", server, res.Status)
		return
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println("FAIL")
		return
	}

	var categories []string
	doc.Find("thead").Each(func(i int, s *goquery.Selection) {
		cat := strings.ToLower(
			strings.ReplaceAll(
				strings.TrimSpace(s.Find("h3").Text()), " ", "_",
			),
		)
		categories = append(categories, cat)
	})

	var urlList [][]string
	doc.Find("tbody").Each(func(i int, s *goquery.Selection) {
		var urls []string
		s.Find("a").Each(func(i int, s *goquery.Selection) {
			href, _ := s.Attr("href")
			urls = append(urls, href)
		})
		urlList = append(urlList, urls)
	})

	fmt.Println("OK")

	data := map[string]any{}
	for i := range categories {
		fmt.Printf("\rFetching endpoints... [%2d/%2d] ", i, len(categories))
		category := categories[i]
		var endpoints []map[string]any
		for _, url := range urlList[i] {
			res, err := endpointRead(url)
			if err != nil {
				fmt.Println("FAIL")
				return err
			}
			endpoints = append(endpoints, res)
		}
		data[category] = endpoints
		fmt.Printf("\rFetching endpoints... [%2d/%2d] ", i+1, len(categories))
	}

	fmt.Println("OK")

	fmt.Print("Writing json... ")
	var jsonBytes []byte
	if pretty {
		jsonBytes, err = json.MarshalIndent(data, "", "  ")
	} else {
		jsonBytes, err = json.Marshal(data)
	}
	if err != nil {
		fmt.Println("FAIL")
		return
	}
	fh, err := os.Create(path)
	if err != nil {
		fmt.Println("FAIL")
		return
	}
	defer fh.Close()
	_, err = fh.Write(jsonBytes)
	if err != nil {
		fmt.Println("FAIL")
		return
	}

	fmt.Println("OK")
	return
}

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "Usage: "+os.Args[0]+" <output_file> [true|false]")
		os.Exit(1)
	}
	var pretty bool
	if len(os.Args) == 3 && os.Args[2] == "true" {
		pretty = true
	} else {
		pretty = false
	}
	if err := run(os.Args[1], pretty); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
