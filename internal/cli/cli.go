package cli

import (
	"fmt"
	"strings"

	"github.com/integrii/flaggy"

	"codeberg.org/infinanis/semblance/internal/api"
)

const version = "v0.0.2 (pre-release)"

var (
	sectionCmds, endpointCmds []*flaggy.Subcommand
	Outfile                   string
	DoYaml                    bool
	Section                   string
	Endpoint                  string
	Args                      map[string]any
)

func init() {
	flaggy.SetVersion(version)
	flaggy.String(&Outfile, "o", "output", "Redirect output to a file (Default: stdout).")

	for section := range api.Data {
		sectionCmd := flaggy.NewSubcommand(section)
		flaggy.AttachSubcommand(sectionCmd, 1)
		sectionCmds = append(sectionCmds, sectionCmd)

		for _, endpoint := range api.Data[section] {
			_, ok := api.ModuleMap[section][endpoint.Name]
			if !ok {
				continue
			}

			endpointCmd := flaggy.NewSubcommand(api.ModuleMap[section][endpoint.Name])
			endpointCmd.Description = endpoint.Description

			for _, param := range endpoint.ParamsOpt {
				desc := param.Description

				var suffix string

				if param.Def != "-" {
					suffix = fmt.Sprintf(" (Default: %s)", param.Def)
				} else if param.Example != "-" {
					suffix = fmt.Sprintf(" (Example: %s)", param.Example)
				}

				switch t := param.ValType; t {
				case "String":
					var flag string

					endpointCmd.String(&flag, param.Name, "", desc+suffix)
				case "Int":
					var flag int

					endpointCmd.Int(&flag, param.Name, "", desc+suffix)
				case "Boolean", "Boolean(0,1)":
					var flag bool

					endpointCmd.Bool(&flag, param.Name, "", desc)
				default:
					if strings.Contains(t, "Enum") {
						desc = fmt.Sprintf(
							"One of: %s; ",
							strings.Replace(t, "Enum", "", 1),
						) + desc
					}

					var flag string

					endpointCmd.String(&flag, param.Name, "", desc)
				}
			}

			for i, param := range endpoint.ParamsReq {
				desc := param.Description
				if param.ValType == "array" {
					desc += " (Values should be separated with a comma)"
				}

				var suffix string

				if param.Example != "-" {
					suffix = fmt.Sprintf(" (Example: %s)", param.Example)
				}

				var flag string

				endpointCmd.AddPositionalValue(
					&flag,
					param.Name,
					i+1,
					true,
					desc+suffix,
				)
			}

			if api.FiletypeMap[section] == api.JSON {
				endpointCmd.Bool(
					&DoYaml,
					"y",
					"yaml",
					"Output data as YAML instead of JSON for better readability.",
				)
			}

			sectionCmd.AttachSubcommand(endpointCmd, 1)
			endpointCmds = append(endpointCmds, endpointCmd)
		}
	}

	flaggy.Parse()

	Section, Endpoint, Args = GetArgs()
}

func GetArgs() (section, endpoint string, args map[string]any) {
	sectionSel, endpointSel := findSelectedCommands()

	if sectionSel == nil || endpointSel == nil {
		flaggy.ShowHelpAndExit("")
	}

	args = make(map[string]any)

	method := findMethod(sectionSel, endpointSel)

	for _, flag := range endpointSel.Flags {
		if val, ok := getFlagValue(flag.AssignmentVar); ok {
			args[flag.ShortName] = val
		}
	}

	for _, flag := range endpointSel.PositionalFlags {
		args[flag.Name] = getSliceValue(flag.AssignmentVar)
	}

	return sectionSel.Name, method, args
}

func findSelectedCommands() (section, endpoint *flaggy.Subcommand) {
	for _, sectionCmd := range sectionCmds {
		if sectionCmd.Used {
			for _, endpointCmd := range endpointCmds {
				if endpointCmd.Used {
					section = sectionCmd
					endpoint = endpointCmd

					break
				}
			}
		}
	}

	return section, endpoint
}

func findMethod(sectionSel, endpointSel *flaggy.Subcommand) string {
	for key, val := range api.ModuleMap[sectionSel.Name] {
		if val == endpointSel.Name {
			return key
		}
	}

	return ""
}

func getFlagValue(assignmentVar any) (any, bool) {
	switch v := assignmentVar.(type) {
	case *string:
		return *v, true
	case *int:
		return *v, true
	case *bool:
		return *v, true
	default:
		return nil, false
	}
}

func getSliceValue(assignmentVar any) []string {
	if v, ok := assignmentVar.(*string); ok {
		return strings.Split(*v, ",")
	}

	return nil
}
