package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const Server = "https://rest.ensembl.org"

var (
	client        *http.Client
	rateLimitWarn bool
)

func init() {
	client = http.DefaultClient
	rateLimitWarn = false
}

func politeRequest(req *http.Request) (*http.Response, error) {
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("request failed: %w", err)
	}

	if resp.StatusCode == http.StatusTooManyRequests {
		secsFloat, _ := strconv.ParseFloat(resp.Header["Retry-After"][0], 64)

		secs := int(math.Ceil(secsFloat))
		if secs > 2 && !rateLimitWarn {
			fmt.Fprintf(
				os.Stderr,
				"\nWARNING: Large API rate limit detected, consider switching to a less busy network\n",
			)

			rateLimitWarn = true
		}

		time.Sleep(time.Duration(secs) * time.Second)

		resp, err = politeRequest(req)
		if err != nil {
			return nil, err
		}
	}

	return resp, nil
}

func paramToString(val any) string {
	switch v := val.(type) {
	case []string:
		return v[0]
	case string:
		return v
	case bool:
		if v {
			return "1"
		}

		return "0"
	default:
		return ""
	}
}

func Do(r Request) (io.ReadCloser, error) {
	path := r.Endpoint.Path

	spl := strings.Split(r.Endpoint.Path, ":")
	if len(spl) > 1 {
		for _, param := range spl[1:] {
			param = strings.TrimSuffix(param, "/")

			var val string

			switch v := r.Params[param].(type) {
			case []string:
				val = v[0]
			case string:
				val = v
			}

			path = strings.Replace(path, ":"+param, val, 1)

			delete(r.Params, param)
		}
	}

	var body string

	var sb strings.Builder

	sb.WriteString(path + "?")

	if r.Endpoint.Method == http.MethodPost {
		bodyBytes, err := json.Marshal(r.Params)
		if err != nil {
			return nil, fmt.Errorf("marshalling request body failed: %w", err)
		}

		body = string(bodyBytes)
	} else {
		for param, val := range r.Params {
			val = paramToString(val)
			if val != "" {
				sb.WriteString(fmt.Sprintf("%s=%s;", param, val))
			}
		}
	}

	req, err := http.NewRequest(
		r.Endpoint.Method,
		Server+sb.String(),
		bytes.NewBufferString(body),
	)
	if err != nil {
		return nil, fmt.Errorf("making request failed: %w", err)
	}

	req.Header.Add("content-type", r.ContentType.String())

	resp, err := politeRequest(req)
	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}
