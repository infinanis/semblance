package api

type MimeType int

const (
	FASTA MimeType = iota
	GFF3
	BED
	JSON
	Newick
	SeqXML
	PhyloXML
	XML
	YAML
)

func (mt MimeType) String() string {
	return []string{
		"text/x-fasta",
		"text/x-gff3",
		"text/x-bed",
		"application/json",
		"text/x-nh",
		"text/x-seqxml+xml",
		"text/x-phyloxml+xml",
		"text/xml",
		"text/x-yaml",
	}[mt]
}

func (mt MimeType) Name() string {
	return []string{
		"FASTA",
		"GFF3",
		"BED",
		"JSON",
		"Newick (New Hampshire) format",
		"SeqXML",
		"PhyloXML",
		"XML",
		"YAML",
	}[mt]
}

func (mt MimeType) Extension() string {
	return []string{
		".fa",
		".gff3",
		".bed",
		".json",
		".nh",
		".seqxml",
		".phyloxml",
		".xml",
		".yaml",
	}[mt]
}
