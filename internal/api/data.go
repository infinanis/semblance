package api

import (
	"embed"
	"encoding/json"
	"fmt"
	"io"
	"os"
)

func init() {
	Data = map[string][]Endpoint{}

	apiMap, err := LoadAPIData()
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}

	for key, val := range apiMap {
		var endpoints []Endpoint

		val, ok := val.([]any)
		if !ok {
			fmt.Fprint(os.Stderr, "Type assertion failed")
			os.Exit(1)
		}

		for _, endpoint := range val {
			jsonBytes, err := json.Marshal(endpoint)
			if err != nil {
				fmt.Fprint(os.Stderr, err)
				os.Exit(1)
			}

			var endpointStruct Endpoint

			if err := json.Unmarshal(jsonBytes, &endpointStruct); err != nil {
				fmt.Fprint(os.Stderr, err)
				os.Exit(1)
			}

			endpoints = append(endpoints, endpointStruct)
		}

		Data[key] = endpoints
	}
}

type Param struct {
	Name, Description, Example string
	ValType                    string `json:"type"`
	Def                        string `json:"default"`
}

type Endpoint struct {
	Name, Description, Method, Path string
	ParamsReq                       []Param `json:"params_req"`
	ParamsOpt                       []Param `json:"params_opt"`
}

type Table map[string][]Endpoint

var Data Table

//go:embed data/api.json
var apiFile embed.FS

func (at Table) FindEndpoint(section, name string) (*Endpoint, error) {
	if _, ok := at[section]; !ok {
		return nil, fmt.Errorf("section '%s' not found", section)
	}

	for _, val := range at[section] {
		if val.Name == name {
			return &val, nil
		}
	}

	return nil, fmt.Errorf("endpoint '%s' not found in '%s'", name, section)
}

func (ae Endpoint) FindParam(name string) (Param, error) {
	var res Param

	for _, val := range append(ae.ParamsReq, ae.ParamsOpt...) {
		if val.Name == name {
			res = val

			return res, nil
		}
	}

	err := fmt.Errorf("earam '%s' not found in endpoint '%s'", name, ae.Name)

	return res, err
}

func LoadAPIData() (map[string]any, error) {
	file, _ := apiFile.Open("data/api.json")
	jsonBytes, _ := io.ReadAll(file)

	var res map[string]any

	if err := json.Unmarshal(jsonBytes, &res); err != nil {
		return nil, fmt.Errorf("failed to unmarshal api.json: %w", err)
	}

	return res, nil
}
