package api

var FiletypeMap = map[string]MimeType{
	"compgen":  JSON,
	"info":     JSON,
	"lookup":   JSON,
	"mapping":  JSON,
	"sequence": FASTA,
	"ontology": JSON,
	"overlap":  JSON,
	"taxonomy": JSON,
}

var ModuleMap = map[string]map[string]string{
	"compgen": {
		"homology_species_gene_id": "homology_id",
		"homology_symbol":          "homology_symbol",
	},
	"info": {
		"species": "species",
	},
	"lookup": {
		"symbol_post": "symbol",
		"lookup_post": "id",
	},
	"mapping": {
		"assembly_cdna":        "cdna2gen",
		"assembly_cds":         "cds2gen",
		"assembly_translation": "prot2gen",
		"assembly_map":         "asm2asm",
	},
	"ontology": {
		"ontology_id":   "id",
		"ontology_name": "name",
	},
	"overlap": {
		"overlap_id":     "id",
		"overlap_region": "region",
	},
	"taxonomy": {
		"taxonomy_classification": "classification",
		"taxonomy_id":             "term",
	},
	"sequence": {
		"sequence_id_post": "id",
		"sequence_region":  "region",
	},
}

type Request struct {
	Endpoint    *Endpoint
	Params      map[string]any
	ContentType MimeType
}

func MakeRequest(
	endpoint *Endpoint,
	params map[string]any,
	contentType MimeType,
) (Request, error) {
	res := Request{
		Endpoint:    endpoint,
		Params:      map[string]any{},
		ContentType: contentType,
	}

	for key, val := range params {
		param, err := endpoint.FindParam(key)
		if err != nil {
			continue
		}

		res.Params[param.Name] = val
	}

	return res, nil
}
