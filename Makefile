GOCMD=GO111MODULE=on CGO_ENABLED=0 go
GOTEST=$(GOCMD) test
GOVET=$(GOCMD) vet
GOFMT=gofumpt -l -w .
LINT=golangci-lint run
TARGET=semblance
PREFIX=/usr/local/bin

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

.PHONY: default run install build build-release table scraper check format lint clean help

default: build

run: ## Compile and run
	@$(MAKE) -s build
	@./bin/$(TARGET)

install: ## Install the binary in $PREFIX
	@install -v -m 0755 bin/$(TARGET) $(PREFIX)

build: format ## Build the project
	mkdir -p bin
	$(GOCMD) build -o bin/$(TARGET) .

build-release: format  ## Build target binaries for multiple operating systems
	mkdir -p bin
	@echo -n "Building for linux/amd64"
	$(GOCMD) build -ldflags '-s' -o bin/$(TARGET)-linux-amd64
	@echo -n "Building for freebsd/amd64"
	$(GOCMD) build -ldflags '-s' -o bin/$(TARGET)-freebsd-amd64
	@echo -n "Building for openbsd/amd64"
	$(GOCMD) build -ldflags '-s' -o bin/$(TARGET)-openbsd-amd64
	@echo -n "Building for darwin/amd64"
	$(GOCMD) build -ldflags '-s' -o bin/$(TARGET)-darwin-amd64
	@echo -n "Building for windows/amd64"
	$(GOCMD) build -ldflags '-s' -o bin/$(TARGET)-windows-amd64.exe

table: scraper ## Raw API lookup table scraped from Ensembl's documentation - requires manual editing due to inconsistencies
	mkdir -p $(DATADIR)
	scraper/bin/scraper api_raw.json true

scraper: ## Build scraper module
	cd scraper && make

check: lint format ## Run linters and formatters

format: ## Format source files
	@$(GOFMT)

lint: ## Lint source files
	@$(LINT)

clean: ## Clean the build directory
	rm -rf bin

help: ## Show this help
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)
