module codeberg.org/infinanis/semblance

go 1.20

require (
	github.com/integrii/flaggy v1.5.2
	github.com/itchyny/json2yaml v0.1.4
)
